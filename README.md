
# Projet étudiants M2 IAFA 1-3



## Algorithme de déconvolution
L'algorithme que je vous propose est très simple, on a l'estimation $\hat{x} = \underset{x}{min} ||h*x - y||^2_2 + \lambda ||x||^2_2$, avec $h$ le noyau de convolution, $y = h * x + n$ l'image dégradée,  $x$ l'image propre,   $n \sim \mathcal{N}(0, \sigma)$ et $\lambda$ le paramètre d'intérêt. 

## Code
Vous trouverez dans ce dépôt :
- une image test (libre à vous de tester d'autres images si vous le souhaiter, ce n'est pas obligatoire et si jamais, je vous conseille de le faire à la fin)
- $\verb+DeconvolutionAlgorithm.m+$  : Comme son nom l'indique, c'est l'algorithme de déconvolution, dont la solution s'écrit $\hat{x} = F^{-1}\left(\frac{\overline{F(h)}}{|F(h)|^2 + \lambda} \odot F(y) \right)$ (la division $\frac{\cdot}{\cdot}$ et la multiplication $\odot$ se font bien sûr point à point dans l'espace de Fourier).  Attention, cette solution n'est correcte que si $y$ est obtenue par convolution avec effets de bord circulaire, soit les effets de bords par défaut dans le domaine de Fourier.
- $\verb+DeconvDemo.m+$ : Simple script de dégradation puis reconstruction de $x$, appelant  $\verb+DeconvolutionAlgorithm+$. Il vous permet de tester rapidement différentes configuration de dégradation et valeurs de $\lambda$ si vous le souhaiter.
- $\verb+FindBestLambda.m+$ : Effectue une recherche de grille afin de trouver le meilleur $\lambda$ possible, $\lambda^*$, et affiche le résultat.

## D'ici le vendredi 23/02 (inclus), vous devez : 
- réécrire l'algorithme de déconvolution ainsi que les deux scripts en PyTorch. 
- vérifier que votre implémentation est correcte, vous devez trouver **EXACTEMENT** les mêmes résultats en PyTorch et MatLab. Attention à l'aléatoire du bruit, générez un $y$, sauvegardez le et utilisez ce même $y$ dans les deux langages.
- écrire un script en PyTorch permettant de retrouver le meilleur $\lambda$ par descente de gradient et non par recherche de grille (peut m'importe l'optimiseur que vous utilisez). Pour commencer, je vous demande de trouver $\lambda^* = \underset{\lambda}{argmin} ||F^{-1}\left(\frac{\overline{F(h)}}{|F(h)|^2 + \lambda} \odot F(y) \right) - x||^2_2$. Pensez à ne pas faire aveuglément confiance à votre optimiseur, et affichez la valeur de votre fonction de coût au cours de l'optimisation, cela vous aidera à régler le *learning rate* et le nombre d'itérations.

## D'ici le vendredi 01/03 (inclus), vous devez :
- comprendre les *loss* utilisées dans vos articles respectifs, ou du moins identifier vos difficultés de compréhension vis à vis de votre article, pour que je puisse y répondre.
- avoir utilisé les *loss* présentés dans vos articles comme fonction de coût non supervisée pour trouver $\hat \lambda$ (commencez par ne pas ajouter de bruit, ça sera sûrement plus simple).

## Si vous voulez aller plus loin :
- Démontrez que $F^{-1}\left(\frac{\overline{F(h)}}{|F(h)|^2 + \lambda} \odot F(y) \right) = \underset{x}{min} ||h*x - y||^2_2 + \lambda ||x||^2_2$.
- Etudiez la relation entre $\lambda^*$ et $\sigma$.
- Etudier la relation entre le  nombre de noyaux de convolutions $h_i$  utilisés et la précision de $\hat \lambda$.




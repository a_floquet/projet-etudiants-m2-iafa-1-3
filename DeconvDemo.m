% Settings
sigma_conv  = 3;
sigma_noise = 1; % set to 0 to have noiseless image
lambda      = 0.06; % set to 0 to see deconvolution wihtout regularization

% Creates images
x = double(imread('im.png'));
h = fspecial('gaussian', size(x), sigma_conv);
n = randn(size(x));
y = fftshift(ifft2(fft2(x) .* fft2(h))) + sigma_noise*n;

% Deconvolution
x_est = DeconvolutionAlgorithm(y, h, lambda);

% Computes scores
psnr_y     = psnr(y, x, max(x(:)));
psnr_x_est = psnr(x_est, x, max(x(:)));

% Display results
figure;
subplot(1,3,1); imagesc(x)    ; title('clean image')                                             ; axis square;
subplot(1,3,2); imagesc(y)    ; title({'blurred image', strcat('psnr=', num2str(psnr_y))})       ; axis square;
subplot(1,3,3); imagesc(x_est); title({'deblurred image', strcat('psnr=', num2str(psnr_x_est))}) ; axis square;
colormap gray

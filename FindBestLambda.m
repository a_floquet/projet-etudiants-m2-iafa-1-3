% Settings
sigma_conv  = 5;
sigma_noise = 2; % set to 0 to have noiseless image
lambda_min = 0;
lambda_max = 0.005;
grid_num   = 1000;

% Creates images
x      = double(imread('im.png'));
h      = fspecial('gaussian', size(x), sigma_conv);
n      = randn(size(x));
y      = fftshift(ifft2(fft2(x) .* fft2(h))) + sigma_noise*n;
psnr_y = psnr(y, x, max(x(:)));

% Deconvolution
lambda_grid = linspace(lambda_min, lambda_max, grid_num);
psnr_gain   = zeros(1,grid_num);
counter     = 1; 
for lambda = lambda_grid
    x_est              = DeconvolutionAlgorithm(y, h, lambda);
    psnr_gain(counter) = psnr(x_est, x, max(x(:))) - psnr_y;
    counter            = counter + 1;
end
[best_gain, best_lambda_idx] = max(psnr_gain);
best_lambda                  = lambda_grid(best_lambda_idx);
best_psnr                    = best_gain + psnr_y;

% Display results
figure; plot(linspace(lambda_min, lambda_max, grid_num), psnr_gain); xlabel('\lambda'); ylabel('Gain de PSNR');

figure;
subplot(1,3,1); imagesc(x)    ; title('clean image')                                                                                   ; axis square;
subplot(1,3,2); imagesc(y)    ; title({'blurred image', strcat('psnr=', num2str(psnr_y))})                                             ; axis square;
subplot(1,3,3); imagesc(x_est); title({'blurred image', strcat('psnr=', num2str(best_psnr)), strcat('\lambda=', num2str(best_lambda))}); axis square;
colormap gray


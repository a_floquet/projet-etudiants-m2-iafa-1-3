function x = DeconvolutionAlgorithm(y, h, lambda)
% Algo de déconvolution simple - résultat = min ||Hx - y||^2_2 + lambda||x||^2_2

% Passage dans Fourier
Y = fft2(y);
H = fft2(h);

% Inversion dans fourier
X = Y .* conj(H) ./ (abs(H).^2 + lambda);
x = fftshift(real(ifft2(X)));
end

